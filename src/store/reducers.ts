import Immutable from 'seamless-immutable';
import { search } from './search/reducers';

export const initialState = Immutable({});

export default {
  search,
};
