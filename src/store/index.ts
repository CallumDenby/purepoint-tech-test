import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import rootReducers, { initialState } from './reducers';
import rootEpic from './epics';

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension'); // eslint-disable-line
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

export default (state = initialState) => {
  const reducers = combineReducers(rootReducers);

  const epicMiddleware = createEpicMiddleware();

  const store = createStore(
    reducers,
    state as any,
    bindMiddleware([epicMiddleware]),
  );

  epicMiddleware.run(rootEpic);

  return store;
};
