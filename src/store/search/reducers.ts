import Immutable from 'seamless-immutable';
import { FETCH_SEARCH_RESULTS_SUCCESS } from './actions';

const initialState = Immutable({});

export const search = (state = initialState, action) => {
  console.log('type', action.type, action.payload);
  switch (action.type) {
    case FETCH_SEARCH_RESULTS_SUCCESS:
      return state.merge({
        results: action.payload,
      });
    default:
      return state;
  }
};
