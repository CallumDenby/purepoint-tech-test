export const FETCH_SEARCH_RESULTS = 'FETCH_SEARCH_RESULTS';
export const FETCH_SEARCH_RESULTS_SUCCESS = 'FETCH_SEARCH_RESULTS_SUCCESS';

export const fetchSearchResults = payload => ({
  payload,
  type: FETCH_SEARCH_RESULTS,
});

export const fetchSearchResultsSuccess = payload => ({
  payload,
  type: FETCH_SEARCH_RESULTS_SUCCESS,
});
