import { FETCH_SEARCH_RESULTS, fetchSearchResultsSuccess } from './actions';
import { ofType } from 'redux-observable';
import { mergeMap , map, debounceTime, toArray } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { from } from 'rxjs';

export interface IResultItem {
  title: string;
  href: string;
  ingredients: string;
  thumbnail: string;
}

interface IResponse {
  title: string;
  version: number;
  href: string;
  results: [IResultItem];
}

const search = ({ payload }) =>
  from([`http://localhost:4000/api?q=${payload}`]).pipe(
    mergeMap(url => [url, `${url}&p=2`]),
    mergeMap(url => ajax.getJSON(url)),
    toArray(),
    map(([first, second]: IResponse[]) => [...first.results, ...second.results]),
  );

export const searchEpic = $action => $action.pipe(
  ofType(FETCH_SEARCH_RESULTS),
  debounceTime(300),
  mergeMap(search),
  map(fetchSearchResultsSuccess),
);
