import { searchEpic } from './search/epics';
import { combineEpics } from 'redux-observable';

export default combineEpics(searchEpic);
