import glamorous from 'glamorous';

export const Container = glamorous.main({
  paddingTop: '3em',
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
});
