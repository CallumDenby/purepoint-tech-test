import g from 'glamorous';

export const Search = g.input({
  height: '1em',
  fontSize: '3em',
  width: '80%',
  maxWidth: '1200px',
  color: '#353b48',
  borderColor: '#40739e',
  borderStyle: 'solid',
  borderWidth: '10px',
  padding: '10px',
  marginBottom: '10px',
});
