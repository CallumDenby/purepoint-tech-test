import g from 'glamorous';

export const Result = g.p({
  fontSize: '1em',
  borderStyle: 'solid',
  borderColor: '#333',
  borderWidth: '10px',
  padding: '10px',
});

export const ResultContainer = g.div({
  // padding: '10px',
  width: 'calc(80% + 10px)',
  maxWidth: '1200px',
  marginBottom: '10px',
});
