import React, { Component } from 'react';
import { Container } from '../components/container';
import { Search } from '../components/searchbox';
import { connect, Dispatch } from 'react-redux';
import { fetchSearchResults } from '../store/search/actions';
import { IResultItem } from '../store/search/epics';
import { Result, ResultContainer } from '../components/result';

interface IProps {
  dispatch: Dispatch;
  results: [IResultItem];
}

export class IndexPage extends Component<IProps> {
  onSearchChange = (event) => {
    this.props.dispatch(fetchSearchResults(event.currentTarget.value));
  }

  renderResults() {
    const { results: data } = this.props;
    if (!data) return null;

    const results = data.map(result => (
      <Result key={result.title}>
        <a href={result.href}>{result.title}</a>
      </Result>
    ));

    return (
      <ResultContainer>
        {results}
      </ResultContainer>
    );
  }

  render() {
    return (
      <Container>
        <Search
          placeholder="Search..."
          onChange={this.onSearchChange}
        />
        {this.renderResults()}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  results: state.search.results,
});

export default connect(mapStateToProps)(IndexPage);
