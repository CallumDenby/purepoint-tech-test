import App, { Container } from 'next/app';
import Immutable from 'seamless-immutable';
import React from 'react';
import { rehydrate, css } from 'glamor';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import { Store } from 'redux';
import createStore from '../store';

interface IWindow extends Window {
  __NEXT_DATA__: {
    ids: any;
  };
}

if (typeof window !== 'undefined') {
  rehydrate((window as IWindow).__NEXT_DATA__.ids);
}

interface IProps {
  Component: React.ComponentType<{}>;
  pageProps: object;
  store: Store;
}

export class Application extends App<IProps> {
  props: IProps;

  static async getInitialProps({ Component, ctx }) {
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps({ ctx })
      : {};

    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;

    css.global('html, body', {
      margin: 0,
      backgroundColor: '#eee',
    });

    return (
      <Container>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    );
  }
}

const withReduxOptions: any = {
  deserializeState: state => Immutable(state),
  serializeState: state => state,
};

export default withRedux(createStore as any, withReduxOptions)(Application);
