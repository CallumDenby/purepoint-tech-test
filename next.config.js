const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const withTypescript = require('@zeit/next-typescript')

const { ANALYZE } = process.env;

module.exports = withTypescript({
  webpack(config, { isServer }) {
    if (ANALYZE) {
      config.plugins.push(new BundleAnalyzerPlugin({
        analyzerMode: 'server',
        analyzerPort: isServer ? 8888 : 8889,
        openAnalyzer: true,
      }));
    }

    config.module.rules.push({
      test: /__tests__/,
      loader: 'ignore-loader'
    });

    return config;
  }
});
